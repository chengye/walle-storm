from setuptools import setup, find_packages


setup(
    name='wall-storm',
    version='0.0.1',
    author='Chengye Zhao',
    author_email='chengyezhao@gmail.com',
    description='simulation tool for walle system',
    url='http://flashhold.com',
    license='MIT License',
    install_requires=[],
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    include_package_data=True
)