# -*- coding: utf-8 -*-
import matplotlib.patches as patches
import matplotlib.pyplot as plt

class MapBuilder:
    def __init__(self):
        self.map = None
        self.width = 0
        self.height = 0

    def load(self, filename):
        self.map = list()
        line_stack = list()
        with open(filename, "r") as f:
            for line in f:
                line_stack.append(line)
        line_stack.reverse()
        self.height = len(line_stack)
        for y in range(len(line_stack)):
            row = list()
            row_data = str.split(line_stack[y], ",")
            self.width = len(row_data)
            for x in range(len(row_data)):
                row.append(WayPoint(x, y, row_data[x]))
            self.map.append(row)

    def get_waypoint_by_coordinate(self, x, y):
        return self.map[y][x]

    def get_waypoint_by_index(self, index):
        y = index / self.width
        x = index % self.width
        return self.map[y][x]

    @staticmethod
    def get_path_str(waypoint_list):
        res = ""
        for wp in waypoint_list:
            res += str(wp) + "->"
        return res


class WayPointType:
    # 工作站等待点
    PICKING_STATION_WAITING = 2
    # 工作站工作点
    PICKING_STATION_WORKING = 3
    # 工作站换向点
    PICKING_STATION_TURNING = 4
    # 存储位置
    STORAGE = 5
    # 路径
    PATH = 6
    # 充电站
    CHARGER_STATION = 7
    # 货架分离点
    BUCKET_DETACHABLE_WORKING = 8
    # 充电点
    CHARGER_POLE = 9
    # 货架入场点
    BUCKET_ENTRANCE = 10
    # 禁止通行点
    # FORBIDDEN
    # 工作站工人所处位置
    # PICKING_STATION_HUMAN
    # 换向区中心点
    # PICKING_STATION_TURNING_CENTER

    @staticmethod
    def get_color(wp_type):
        if wp_type == WayPointType.PICKING_STATION_WAITING:
            return "#BFEFFF"
        elif wp_type == WayPointType.PICKING_STATION_WORKING:
            return "#F08080"
        elif wp_type == WayPointType.PICKING_STATION_TURNING:
            return "#00868B"
        elif wp_type == WayPointType.STORAGE:
            return "#71C671"
        elif wp_type == WayPointType.PATH:
            return "#f1f1f1"
        elif wp_type == WayPointType.CHARGER_STATION:
            return "#F08080"
        elif wp_type == WayPointType.BUCKET_DETACHABLE_WORKING:
            return "#F08080"
        elif wp_type == WayPointType.CHARGER_POLE:
            return "#5D478B"
        elif wp_type == WayPointType.BUCKET_ENTRANCE:
            return "#F08080"
        else:
            return "#000000"


class WayPoint:
    def __init__(self, x, y, attribute):
        self.x = x
        self.y = y
        self.id = WayPoint.point2id(self.x, self.y)
        attribute_list = str.split(attribute, "|")
        try:
            type_str = attribute_list[0]
            # self.type = WayPointType(int(type_str))
            self.type = int(type_str)
            self.color = WayPointType.get_color(int(type_str))
            access_weights_str = attribute_list[1]

            station_id_str = attribute_list[2]

            picking_face_str = attribute_list[3]

            picking_station_index_str = attribute_list[4]

            turning_hub_offset_str = attribute_list[5]

        except Exception, e:
            pass

    def __str__(self):
        return "(%d,%d)" % (self.x, self.y)

    def get_render_patches(self, sq_width, sq_height):
        return patches.Rectangle((self.x, self.y), sq_width, sq_height, fill=True, facecolor=self.color,
                                 edgecolor='gray', linewidth=0.5, linestyle="solid")
    @staticmethod
    def id2point(id):
        return 0, 0

    @staticmethod
    def point2id(x, y):
        return 0

    def get_adjacent(self, index):
        """
        
        :param index: 相邻点索引值
        :return: 相邻点
        """
        return None

    def get_cost_with_shelf(self, index):
        """
        
        :param index: 相邻点索引值
        :return: 带货架移动代价 0代表不可移动
        """
        return 0

    def get_cost_without_shelf(self, index):
        """
        
        :param index: 相邻点索引值
        :return: 不带货架移动代价 0代表不可移动
        """
        return 0

    def get_available_adjacent_number_with_shelf(self):
        """
        
        :return: 带货架可以一步到达的邻接点个数
        """
        return 0

    def get_available_adjacent_number_without_shelf(self):
        """
        
        :return:  
        """
        return 0

    def get_distance(self, end):
        """
        
        :param end: 目标终点
        :return: 返回到目标终点的曼哈顿距离
        """
        return abs(self.x - end.x) + abs(self.y - end.y)


if __name__ == "__main__":
    test_map = MapBuilder()
    test_map.load("map_export.csv")
    terra_width = 1
    figure = plt.figure()
    ax = figure.add_subplot(1, 1, 1, aspect='equal')
    ax.set_xlim(0, test_map.width * terra_width)
    ax.set_ylim(0, test_map.height * terra_width)
    while True:
        ax.cla()
        for row in test_map.map:
            for wp in row:
                ax.add_patch(wp.get_render_patches(1, 1))
        plt.pause(1)
    pass
