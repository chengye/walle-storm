
Walle_Time_Tick_Event = "walle_time_tick_event"
Walle_Init_Event = "walle_init_event"

def build_init_event():
    return SimpleEvent(0, 0, Walle_Init_Event,
                       "world", None, 0)

def build_time_tick_event(tick):
    return SimpleEvent(tick, tick, Walle_Time_Tick_Event,
                       "world", None, tick)

class SimpleEvent:
    def __init__(self, create_time, trigger_time, event_type, source, target, message):
        self.create_time = create_time
        self.trigger_time = trigger_time
        self.event_type = event_type
        self.target = target
        self.message = message
        self.source = source

    def dict_to_string(self, dict):
        s = ""
        for (key, item) in dict.items():
            s = s + str(key) + "=" + str(item) + ";"
        return s

    def __str__(self):
        if self.message.__class__ is dict:
            return ("   %s  \t  %s  \t  %s  \t  %s  \t  %s  \t  %s" % (self.create_time, self.trigger_time, self.event_type, self.source, self.target, self.dict_to_string(self.message)))
        else:
            return ("   %s  \t  %s  \t  %s  \t  %s  \t  %s  \t  %s" % (self.create_time, self.trigger_time, self.event_type, self.source, self.target, str(self.message)))